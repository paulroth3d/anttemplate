Ant Deployment
==============
V0.9

Contributors:
Trey Roldan	- troldan@modelmetrics.com
Paul Roth	- proth@modelmetrics.com

The purpose of this project is to provide a simple but powerful interface for
creating, building and deploying with ant.

##Installing Ant##

If ant is not currently installed, the simplest way to install ant is either through [macports](http://www.macports.org/) or [homebrew](http://mxcl.github.com/homebrew/).

Both are fairly simple and are similar to

	sudo port install apache-ant
	
or

	brew install apache-ant

TO RUN:
-------

Requires ant installed. See below to install apache ant.

Once checked out, run

	>ant setup
	
Ant Targets are peices of functionality that ant can perform. High level targets direct to the appropriate functionality needed and should be given greater attention.
	
To see the list of different targets, type the following command:
	>ant listAntTargets
	
To run a specific target, simply type the following with one of the targets:

	$>ant [[TARGET]]
	
Such as

	$>ant help
	
#Quick Start#

###To retrieve all classes, triggers, profiles, objects and workflows:###

1. Create a new package through `ant newPackage`
2. Add those package types through `ant addPackageTypes`<br />
	(providing "ApexClass, ApexTrigger, CustomObject Workflow` )<br />
	**This adds all package members of each of those types to unpackaged/package.xml**
3. Retrieve the metadata through `ant retrievePackage`

Note: 

-	Members can be added individually: `ant addPackageMember`
-	All members of a metadata type can be added: `ant addPackageType`
-	All members needed for documentation: `ant addDocPackageTypes`
-	All members available can be added automatically: `ant addAllPackageTypes`

###To create automated reports###

1. Create a new package through `ant newPackage` (or modify packages)
2. Add all package types by `ant addDocPackageTypes`
3. Retrieve the metadata through `ant retrievePackage`
4. Run the documentation on retrieved metadata: `ant doc`<br/>
	*(only types retrieved are shown in reports. To hide/show a class/type/etc - make sure it is retrieved or not retrieved as desired)*

Quick note on ant-salesforce.jar
=====================================================
This uses a 'FIXED' version of the ant-salesforce.jar
(One with sforce_ant.properties supplied)
Please see the ('Salesforce ant-salesforce.jar') section
for more information
=====================================================

Targets
=======

To see the list of different targets, type the following command:
	>ant listAntTargets
	
To run a specific target, simply type the following with one of the targets:

	$>ant [[TARGET]]
	
Such as

	$>ant help



High Level targets:
-------------------

	testCredentials         - prompts for username and credentials to verify that they could be used
	
	test                    - Validates to make sure that ANT will work with your system
	
	help                    - Provides this information and current property values (useful for debugging)
	
	prop                    - Outputs the current ant configuration settings

	listAntTargets          - Lists the high level targets
	
	listAllAntTargets       - Lists all the targets available

	newSource               - Creates a new source file to contain credentials for retreiving files
	
	newTarget               - Creates a new target file to contain credentials for deploying files

	clean				    - cleans current project
	
	list                    - Lists various types of information
	
	settings                - Changes build environment settings
	
	manageRetrieve          - Common activities to use against the current retrieve
	
	managePackage           - Common activities to use against the current package
	
	modifyPackage           - SimpleTask - Package modification targets
	
	retrieve                - Retrieves files from SalesForce (such as files or packages)
	
	deploy                  - Completes a deployment (or prepares a deployment)
	
	doc                     - Creates documentation reports

NOTE: The target run when running simply '>ant' is help

Most of the functionality can be found by using these targets, the additional targets allow functionality to be called directly

Additional targets
------------------

    listSources             - lists all the available source files available to pull data from
								(use newSource to define a new one or overwrite an existing target)
    listTargets             - lists all the available target files available to push data to
								(use newTarget to define a new one or overwrite an existing target)
	
	listMetadataTypes       - lists the metadata types available from your org
	
	listMetadataFiles       - Lists all the items for a particular metadata type
	
	listCurrentPackage      - Shows the contents of the current package file
	
	listPackageToChange     - Shows the contents of the current package to modify
	
	listCheckoutDifferences - Lists the current modified files in the checkout directory

	listDifferencesFromSVN  - Performs a diff against the svn.diffFromURL and svn.diffToURL
                               to describe the changes that occured
	
	
	
	
	changeBuildSource       - Changes the server to retrieve from in the build.environment file
	
	changeBuildTarget       - Changes the server to deploy to in the build.environment file
	
	changeBuildSvnURL       - Changes the subversion urls used to create packages in newPackageFromSVN
	
	changeIsTestDeploy      - Changes whether only test deployments occur
	
	changePackageToChange   - Change the target of package modification targets
	
	changeSvnTrunk          - Change the current repository trunk url (note: package.xml expected at the base)
	
	changeSfdcVersion       - Change the SFDC version used in packages
	
	
	
	
	collapsePackage         - collapses a package xml file to prefix members by their type (useful in calculating differences)
	
	expandPackage           - inflates a collapsed package file to standard xml
	
	
	
	listRetrieve            - Lists the retrieved package folders (or shelved retrieved package folders)
	
	storeRetrieve          - Copies the retrieved directory [${retrieve.dir}] to allow temporarily working on a different group of files.
	
	loadRetrieve            - Replaces the the retrieved directory [${retrieve.dir}] with a shelved copy to resume work
	
	removeRetrieve          - Removes a shelved/copied retrieve directory if no longer in use
	
	
	
	listPackages            - Lists the currently available packages [${unpackaged.dir}]
	
	storePackage            - Stores the current package to a separate flile to load later
	
	loadPackage             - Loads a stored package
	
	removePackage           - Removes a stored package if is no longer needed
	
	
	
	addDocPackageTypes      - Adds all metadata types needed for generating documentation.
	
	addAllPackageTypes      - Adds available types to the package (similar to eclipse all metadata)
	
	newPackage              - creates a new package file
	
	newPackageFromSVN       - Generates a package from svn
                    			(uses the svn.diffFromURL and svn.diffToURL from the build.environment)
	
	addPackageMember        - Adds a member to the packageToModify (adding the type if necessary and avoiding duplicates)
	
	removePackageMember     - Removes a metadata member from the package xml
	
	addPackageType          - Retrieves a list of current members of a type to automatically add to the package
	
	removePackageType       - Remove a metadata type from the package xml
	
	
	
	
	comparePackages         - Compares one package to another
	
	combinePackages         - Combines two packages
	
	subtractPackages        - Subttracts one package from another
	
	addStandardObjects      - Adds standard objects to CustomObjects
	
	
	
	
	
	retrieveFile            - Convenience function to retrieve a specific file with helpers to list different metadata types and list of files
	
	retrievePackage         - Retrieves from source using unpackaged/package.xml
	
	retrieveCheckout        - Retrieves the files from the trunk repository directory with the files refreshed from salesforce
	
	updateCheckout          - Updates the checkout directory
	
	refreshCheckout         - Refreshes the files in the checkout directory with the latest files from salesforce
	
	
	
	
	
	applyPredeployment      - Applies the predeploy script to the retrieved files (in retrieve.dir)
	
	
	buildZip                - Binds the retrieval into a zip directory
	
	
	deployPackage           - Deploys the items in the retrieved folder
	
	deployZip               - Deploys a Deployment zip
	
	
	
	
	retrieveApplyDeploy     - Retrieves the metadata, applies the predeployment and deploys it
	
	retrieveApplyZip        - Retrieves the metadata, Applies Predeployment, and Zips it up
	
	
	
	cleanOutput             - clears the file output folder
	
	cleanRetrieved          - clears the current set of retrieved files (note: backups remain)
	
	cleanDocs               - cleans the generated documentation directory
	
	cleanAll                - cleans all the above
	
	
	
	
	retrieveProfiles - Retrieves profiles and objects to allow profile reports to be run
	
	crud                    - Creates a CRUD CSV
    
	docObjects              - Runs an XSLT report on all objects
    
    docProfiles             - Runs an XSLT report on all profiles
    
    docAll                  - Runs all XSLT reports (against objects and profiles)
    
    
    
    
    
    clearApexClass          - Clears all the apex from a specific class in the retrieved folder
    (useful for deleting fields used in apex in an automated fashion)

    
    clearAllApex            - Clears all the apex from all classes in the retrieved folder 	(useful for deleting fields used in apex in an automated fashion)
    
    
    
    
    
    clearApexTrigger        - Clears all the apex from a specific trigger in the retrieved folder
    
    clearAllTriggers        - Clears all the apex from all triggers in the retrieved folder
    

Description of Files
====================

General project files 
---------------------

	build.xml - the ant file
	
	build.properties  - INSTALLATION INDEPENDENT property file that describes how ant should behave for the project
	build.environment - INSTALLATION/COMPUTER DEPENDENT property file that changes depending on the person / computer
	
	source.XXX - files specific for retrieving data from
	target.XXX - files specific for DEPLOYING TO

NOTE: The reason for the difference also provides an additional barrier against accidental deployment in reverse order.
	
	lib        - directory to store jars used by ANT
	
	unpackaged - directory to store the unpackaged 'package.xml' for retrieval and deployment
	
	testPkg    - directory used to test deployments using >ant test

Project specific files
----------------------

	ManualMove - ignored folder to place any files used with applyPredeployment scripts if the retrieved must change before bunding
	
	target.dev.predeploy.sh - DEFINED IN target.XXX
	                          script to execute in applyPredeployment to massage metadata
	                          such as before a deployment
	
Generated directories
---------------------
	output.dir   - directory that metadata lists are stored in to avoid clutter
	retrieve.dir - directory that metadata retrieved is stored in.
	
NOTE: This version bundles the salesforce-ant.jar, and does not require it to be installed.
	
This means that each installation can have a different jar file (if needed) and they will all play nicely with each other
	
Salesforce ant-salesforce.jar
=============================

Although the ant-salesforce.jar file (sent from salesforce) has the antlib.xml file,
they didn't add in a .properties file. This is needed so the tasks can be defined
in the ant build file.

	<taskdef resource="com/salesforce/sforce_ant.properties" uri="http://com.salesforce">
		<classpath>
			<pathelement location="lib/ant-salesforce.jar" />
		</classpath>
	</taskdef>

This means that the user doesn't need to install the ant-salesforce.jar anywhere
(reducing the risk of duplicate jars out or being out of date) or needing to
specify the library file each time and quite simplifies the experience.

If a new jar is needed...
-------------------------

There is a build file to 'fix' it in the lib directory

Simply go to the lib directory
and run ant

	EX:
	$>cd lib
	$>ant

The proerty file added is similar to the following
__________________________________________________

	compileAndTest=com.salesforce.ant.CompileAndTest
	deploy=com.salesforce.ant.DeployTask
	retrieve=com.salesforce.ant.RetrieveTask
	bulkRetrieve=com.salesforce.ant.BulkRetrieveTask
	listMetadata=com.salesforce.ant.ListMetadataTask
	describeMetadata=com.salesforce.ant.DescribeMetadataTask