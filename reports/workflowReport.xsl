<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sfdc="http://soap.sforce.com/2006/04/metadata"
	exclude-result-prefixes="sfdc"
>
<xsl:template match="/">
<html>
<head>
<style type='text/css'>
	.section { padding-left: 30px; }
	.subsection { padding-left: 50px; }
	.subscript { margin-top: -1em; }
</style>
</head>
<body>
	<div id='toc' class='section'>
		<h1>Table of Contents</h1>
		<ul>
			<li><a href='#general'>General</a></li>
			<li><a href='#alerts'>Alerts</a></li>
			<li><a href='#fieldUpdates'>Field Updates</a></li>
			<li><a href='#arules'>Active Rules</a></li>
			<li><a href='#irules'>Inactive Rules</a></li>
		</ul>
	</div>
	<div id="general" class="section">
		<h1>General</h1>
		<table class="general" border="1">
			<tr><th>Header</th><th>Value</th></tr>
			<!-- <tr><td>header</td><th>Value</th></tr> -->
		</table>
		<div class="tocLink"><a href="#toc">Back to Top</a></div>
	</div>
	<div id="alerts" class="section">
		<h1>Alerts</h1>
		
		<xsl:for-each select='sfdc:Workflow/sfdc:alerts'>
			<h2 class='alertFullName'><xsl:attribute name="id"><xsl:value-of select="concat('_', sfdc:fullName)" /></xsl:attribute><xsl:value-of select='sfdc:fullName' /></h2>
			<table border='1'>
				<tr><td>Template</td><td><xsl:value-of select="sfdc:template" /></td></tr>
				<tr><td>Sender Type</td><td><xsl:value-of select="sfdc:senderType" /></td></tr>
				<tr><td>Protected</td><td><xsl:value-of select="sfdc:protected" /></td></tr>
			</table>
			<p>CC'd users: <b><xsl:value-of select="sfdc:ccEmails" /></b></p>
			<p><xsl:value-of select="sfdc:description" /></p>
		</xsl:for-each>
		
		<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	<div id="fieldUpdates" class="section">
		<h1>Field Updates</h1>
		
		<xsl:for-each select='sfdc:Workflow/sfdc:fieldUpdates'>
			<h2 class='fieldUpdateFullName'><xsl:attribute name="id"><xsl:value-of select="concat('_', sfdc:fullName)" /></xsl:attribute><xsl:value-of select='sfdc:name' /></h2>
			<p class='subscript'>(<xsl:value-of select='sfdc:fullName' />)</p>
			<xsl:choose>
				<xsl:when test="sfdc:operation = 'Formula'">
					<p>Sets field <b><xsl:value-of select='sfdc:field' /></b> to the following formula:</p>
					<pre class='formula'><xsl:value-of select='sfdc:formula' /></pre>
				</xsl:when>
				<xsl:otherwise>
					<p>Sets field <b><xsl:value-of select='sfdc:field' /></b> to the literal value: <b><xsl:value-of select='sfdc:literalValue' /></b></p>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		
		<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	<div id="arules" class="section">
		<h1>Active Rules</h1>
		
		<xsl:for-each select="sfdc:Workflow/sfdc:rules[sfdc:active='true']">
			<h2 class='alertFullName'><xsl:attribute name="id"><xsl:value-of select="concat('r_', sfdc:fullName)" /></xsl:attribute><xsl:value-of select='sfdc:fullName' /></h2>
			<table border='1'>
				<tr><td>Active</td><td><xsl:value-of select="sfdc:active" /></td></tr>
				<tr><td>Trigger Type</td><td><xsl:value-of select="sfdc:triggerType" /></td></tr>
			</table>
			<p><xsl:value-of select="sfdc:description" /></p>
			
			<xsl:if test="sfdc:actions">
				<ul>
				<xsl:for-each select="sfdc:actions">
					<li><xsl:value-of select="sfdc:type" /> - <a><xsl:attribute name="href"><xsl:value-of select="concat('#_', sfdc:name)" /></xsl:attribute><xsl:value-of select="sfdc:name" /></a></li>
				</xsl:for-each>
				</ul>
			</xsl:if>
			<xsl:if test="sfdc:formula">
				<p>Uses the following formula:</p>
				<pre><xsl:value-of select="sfdc:formula" /></pre>
			</xsl:if>
			<xsl:if test="sfdc:criteriaItems">
				<p>The workflow is fired when the following conditions occur</p>
				<ul>
				<xsl:for-each select="sfdc:criteriaItems">
					<li><xsl:value-of select="sfdc:field" /> - <xsl:value-of select="sfdc:operation" /> - [<xsl:value-of select="sfdc:value" />]</li>
				</xsl:for-each>
				</ul>
			</xsl:if>
		</xsl:for-each>
		
		<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	<div id="irules" class="section">
		<h1>Inactive Rules</h1>
		
		<xsl:for-each select="sfdc:Workflow/sfdc:rules[sfdc:active='true']">
			<h2 class='alertFullName'><xsl:attribute name="id"><xsl:value-of select="concat('r_', sfdc:fullName)" /></xsl:attribute><xsl:value-of select='sfdc:fullName' /></h2>
			<table border='1'>
				<tr><td>Active</td><td><xsl:value-of select="sfdc:active" /></td></tr>
				<tr><td>Trigger Type</td><td><xsl:value-of select="sfdc:triggerType" /></td></tr>
			</table>
			<p><xsl:value-of select="sfdc:description" /></p>
			
			<xsl:if test="sfdc:actions">
				<ul>
				<xsl:for-each select="sfdc:actions">
					<li><xsl:value-of select="sfdc:type" /> - <a><xsl:attribute name="href"><xsl:value-of select="concat('#_', sfdc:name)" /></xsl:attribute><xsl:value-of select="sfdc:name" /></a></li>
				</xsl:for-each>
				</ul>
			</xsl:if>
			<xsl:if test="sfdc:formula">
				<p>Uses the following formula:</p>
				<pre><xsl:value-of select="sfdc:formula" /></pre>
			</xsl:if>
			<xsl:if test="sfdc:criteriaItems">
				<p>The workflow is fired when the following conditions occur</p>
				<ul>
				<xsl:for-each select="sfdc:criteriaItems">
					<li><xsl:value-of select="sfdc:field" /> - <xsl:value-of select="sfdc:operation" /> - [<xsl:value-of select="sfdc:value" />]</li>
				</xsl:for-each>
				</ul>
			</xsl:if>
		</xsl:for-each>
		
		<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>
