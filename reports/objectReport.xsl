<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sfdc="http://soap.sforce.com/2006/04/metadata"
	exclude-result-prefixes="sfdc"
>
<xsl:template match="/">
<html>
<head>
<style type='text/css'>
	.section { padding-left: 30px; }
	.subsection { padding-left: 50px; }
</style>
</head>
<body>
	<div id='toc' class='section'>
		<h1>Table of Contents</h1>
		<ul>
			<li><a href='#general'>General</a></li>
			<li><a href='#lookups'>Lookups/Master Detail</a></li>
			<li><a href='#fields'>Fields</a></li>
			<li><a href='#fieldSets'>Field Sets</a></li>
			<li><a href='#validActive'>Validation Rules: Active</a></li>
			<li><a href='#validInactive'>Validation Rules: Inactive</a></li>
		</ul>
	</div>
	<div id='general' class='section'>
		<h1>General</h1>
		<table class='general' border='1'>
			<tr><th>Type</th><th>Is Enabled</th></tr>
			<tr><td>Activities</td><td><xsl:value-of select='sfdc:CustomObject/sfdc:enableActivities' /></td></tr>
			<tr><td>Enhanced Lookup</td><td><xsl:value-of select='sfdc:CustomObject/sfdc:enableEnhancedLookup' /></td></tr>
			<tr><td>enableFeeds</td><td><xsl:value-of select='sfdc:CustomObject/sfdc:enableFeeds' /></td></tr>
			<tr><td>enableHistory</td><td><xsl:value-of select='sfdc:CustomObject/sfdc:enableHistory' /></td></tr>
			<tr><td>enableReports</td><td><xsl:value-of select='sfdc:CustomObject/sfdc:enableReports' /></td></tr>
		</table>
		<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	
	<div id='lookups' class='section'>
		<h1>Lookups</h1>
		<table class='lookups' border='1'>
			<tr>
				<th>Type</th>
				<th>Label</th>
				<th>API</th>
				<th>Reference To</th>
				<!--<th>Relationship Label</th>-->
				<th>Relationship Name</th>
				<th>Required</th>
			</tr>
			<xsl:for-each select="sfdc:CustomObject/sfdc:fields[sfdc:type='Lookup']">
				<xsl:sort select="sfdc:fullName" />
				<tr>
					<td>Lookup</td>
					<td><xsl:value-of select='sfdc:label' /></td>
					<td><xsl:value-of select='sfdc:fullName' /></td>
					<td><xsl:value-of select='sfdc:referenceTo' /></td>
					<td><xsl:value-of select='sfdc:relationshipLabel' /></td>
					<td><xsl:value-of select='sfdc:relationshipName' /></td>
					<td><xsl:value-of select='sfdc:required' /></td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="sfdc:CustomObject/sfdc:fields[sfdc:type='MasterDetail']">
				<xsl:sort select="sfdc:fullName" />
				<tr>
					<td>MasterDetail</td>
					<td><xsl:value-of select='sfdc:label' /></td>
					<td><xsl:value-of select='sfdc:fullName' /></td>
					<td><xsl:value-of select='sfdc:referenceTo' /></td>
					<!--<td><xsl:value-of select='sfdc:relationshipLabel' /></td>-->
					<td><xsl:value-of select='sfdc:relationshipName' /></td>
					<td><xsl:value-of select='sfdc:required' /></td>
				</tr>
			</xsl:for-each>
		</table>
	</div>
	
	<div id='fieldSets' class='section'>
		<h1>Field Sets</h1>
		<xsl:for-each select='sfdc:CustomObject/sfdc:fieldSets'>
			<h2 class='fullName'><xsl:value-of select='sfdc:fullName' /></h2>
			<div class='descr'><pre>Description: <xsl:value-of select='sfdc:description' /></pre></div>
			
			<ul>
			<xsl:for-each select="./sfdc:displayedFields">
				<xsl:sort select="sfdc:field" />
				<xsl:choose>
					<xsl:when test='sfdc:isRequired=false'>
				<li class='req'><xsl:value-of select='sfdc:field' />*</li>
					</xsl:when>
					<xsl:otherwise>
				<li class='noreq'><xsl:value-of select='sfdc:field' /></li>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			</ul>
		</xsl:for-each>
		<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	
	<div class='section'>
	<h1>Fields</h1>
	
	<div id='fields' class='subsection'>
	<h2>Fields</h2>
		<table border='1' cellpadding='1'>
			<tr>
				<th>Field Name</th>
				<th>Label</th>
				<th>Data Type</th>
				<th>PickList Values / Formula / Summary</th>
				<th>Default</th>
				<th>Alpha Sort?</th>
				<th>Standard ?</th>
				<th>Extern ?</th>
				<th>Reqd ?</th>
				<th>Uniq ?</th>
				<th>Hist ?</th>
				<th>Comments</th>
				<th>Default Hidden</th>
			</tr>
			<xsl:for-each select="sfdc:CustomObject/sfdc:fields">
				<xsl:sort select="sfdc:fullName" />
				<tr >
					<td><xsl:value-of select="sfdc:fullName" /></td>
					<td><xsl:value-of select="sfdc:label" /></td>
					<td>
					<xsl:choose>
						<xsl:when test="sfdc:length" >
							<xsl:value-of select="sfdc:type" />[<xsl:value-of select="sfdc:length" />]
						</xsl:when>
						<xsl:when test="sfdc:type = 'Lookup'">Lookup - <xsl:value-of select="sfdc:referenceTo" /></xsl:when>
						<xsl:when test="sfdc:type = 'MasterDetail'">MasterDetail - <xsl:value-of select="sfdc:referenceTo" /></xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="sfdc:type" />
						</xsl:otherwise>
					</xsl:choose>
					</td>
					<td>
					<xsl:choose>
						<xsl:when test="sfdc:type = 'Picklist'">
							<xsl:for-each select="sfdc:picklist/sfdc:picklistValues">
								<xsl:value-of select="sfdc:fullName" />
								<xsl:if test="sfdc:default = 'true'"> - default</xsl:if>
								<xsl:text disable-output-escaping="yes"><br /></xsl:text>
							</xsl:for-each>
						</xsl:when>
						<xsl:when test="sfdc:formula">               
							<xsl:value-of select="sfdc:formula" />
						</xsl:when>
						<xsl:when test="sfdc:type = 'Summary'">
							<xsl:value-of select="sfdc:summaryOperation" /> - <b><xsl:value-of select="sfdc:summarizedField" /></b> on <b><xsl:value-of select="sfdc:summaryForeignKey" /></b>
						</xsl:when>
						<xsl:otherwise></xsl:otherwise>
					</xsl:choose>
					<!--
					picklist: picklist.picklistValues(fullName,default)&#13;summary:summarizedField,summaryForeignKey,summaryOperation&#13;function: formula
					-->
					</td>
					
					<td><xsl:value-of select="sfdc:defaultValue" /></td>
					<td />
					<td>
						<xsl:choose>
							<xsl:when test="contains( sfdc:fullName, '__c' )">false</xsl:when>
							<xsl:otherwise>true</xsl:otherwise>
						</xsl:choose>
					</td>
					<td><xsl:value-of select="sfdc:externalId" /></td>
					<td><xsl:value-of select="sfdc:required" /></td>
					<td><xsl:value-of select="sfdc:unique" /></td>
					<td><xsl:value-of select="sfdc:trackHistory" /></td>
					<td><xsl:value-of select="sfdc:description" /></td>
					<td />
				</tr>
			</xsl:for-each>
		</table>
	</div>
	</div>
	
	<div class='section'>
	<h1>Validation Rules</h1>
	
	<div id='validActive' class='subsection'>
	<h2>Active</h2>
	<xsl:for-each select="sfdc:CustomObject/sfdc:validationRules[sfdc:active='true']">
		<xsl:sort select="sfdc:active" />
		<div class='validation'>
			<h3><xsl:value-of select='sfdc:fullName' /></h3>
			<p class='desc'><xsl:value-of select='sfdc:description' /></p>
			<p class='msg'>Error Message: <xsl:value-of select='sfdc:errorMessage' /></p>
			<pre class='formula'><xsl:value-of select='sfdc:errorConditionFormula' /></pre>
		</div>
	</xsl:for-each>
	<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	
	<div id='validInactive' class='subsection'>
	<h2>Inactive</h2>
	<xsl:for-each select="sfdc:CustomObject/sfdc:validationRules[sfdc:active!='true']">
		<xsl:sort select="sfdc:active" />
		<div class='validation'>
			<h3><xsl:value-of select='sfdc:fullName' /></h3>
			<p class='desc'><xsl:value-of select='sfdc:description' /></p>
			<p class='msg'>Error Message: <xsl:value-of select='sfdc:errorMessage' /></p>
			<pre class='formula'><xsl:value-of select='sfdc:errorConditionFormula' /></pre>
		</div>
	</xsl:for-each>
	<div class='tocLink'><a href='#toc'>Back to Top</a></div>
	</div>
	</div>
	
</body>
</html>
</xsl:template>
</xsl:stylesheet>
