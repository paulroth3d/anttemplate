<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:sfdc="http://soap.sforce.com/2006/04/metadata"
	exclude-result-prefixes="sfdc"
>
<xsl:template match="/">
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Paul Roth</Author>
  <LastAuthor>Paul Roth</LastAuthor>
  <Created>2012-07-23T21:58:33Z</Created>
  <LastSaved>2012-07-23T22:50:57Z</LastSaved>
  <Company>salesforce.com</Company>
  <Version>12.0</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>23320</WindowHeight>
  <WindowWidth>38240</WindowWidth>
  <WindowTopX>4660</WindowTopX>
  <WindowTopY>26480</WindowTopY>
  <Date1904/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Verdana"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s21">
   <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders/>
   <Font x:Family="Swiss" ss:Size="8.0"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders/>
   <Font x:Family="Swiss" ss:Size="8.0"/>
  </Style>
  <Style ss:ID="s24">
   <Font x:Family="Swiss" ss:Size="8.0"/>
  </Style>
  <Style ss:ID="s25">
   <Alignment ss:Vertical="Top"/>
   <Font x:Family="Swiss" ss:Size="20.0" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s26">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0" ss:Bold="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s27">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0" ss:Bold="1"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s28">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0"/>
  </Style>
  <Style ss:ID="s29">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0"/>
  </Style>
  <Style ss:ID="s30">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0"/>
  </Style>
  <Style ss:ID="s31">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s32">
   <Alignment ss:Horizontal="Center" ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s33">
   <Alignment ss:Vertical="Top" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font x:Family="Swiss" ss:Size="8.0"/>
   <Interior/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Sheet1">
  <Table x:FullColumns="1"
   x:FullRows="1" ss:DefaultColumnWidth="53.0">
   <Column ss:StyleID="s21" ss:AutoFitWidth="0" ss:Width="104.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="215.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="145.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="73.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="360.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="176.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="59.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="41.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="38.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="30.0"/>
   <Column ss:StyleID="s23" ss:AutoFitWidth="0" ss:Width="26.0" ss:Span="1"/>
   <Column ss:Index="13" ss:StyleID="s23" ss:AutoFitWidth="0" ss:Width="281.0"/>
   <Column ss:StyleID="s22" ss:AutoFitWidth="0" ss:Width="71.0"/>
   <Column ss:StyleID="s24" ss:AutoFitWidth="0" ss:Width="42.0"/>
   <Row ss:AutoFitHeight="0" ss:Height="23.0" ss:StyleID="s24">
    <Cell ss:StyleID="s25"><Data ss:Type="String">&lt;ObjectName&gt;</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Field Name</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Label</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Data Type</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">PickList Values / Formula / Summary</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Default</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Alpha Sort?</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Standard&#13;?</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Extern&#13;?</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="String">Reqd?</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="String">Uniq?</Data></Cell>
    <Cell ss:StyleID="s27"><Data ss:Type="String">hist&#13;?</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Comments</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Default Hidden</Data></Cell>
   </Row>
   <xsl:for-each select="sfdc:CustomObject/sfdc:fields">
   <xsl:sort select="sfdc:fullName" />
   <Row >
    <Cell ss:Index="2" ss:StyleID="s28"><Data ss:Type="String"><xsl:value-of select="sfdc:fullName" /></Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="String"><xsl:value-of select="sfdc:label" /></Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="String">
    <xsl:choose>
    	<xsl:when test="sfdc:length" >
    		<xsl:value-of select="sfdc:type" />[<xsl:value-of select="sfdc:length" />]
    	</xsl:when>
    	<xsl:when test="sfdc:formula">Formula: <xsl:value-of select="sfdc:type" /></xsl:when>
    	<xsl:when test="sfdc:type = 'Lookup'">Lookup &lt;<xsl:value-of select="sfdc:referenceTo" />&gt;</xsl:when>
    	<xsl:when test="sfdc:type = 'MasterDetail'">MasterDetail &lt;<xsl:value-of select="sfdc:referenceTo" />&gt;</xsl:when>
    	<xsl:otherwise>
    		<xsl:value-of select="sfdc:type" />
    	</xsl:otherwise>
    </xsl:choose>
    </Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="String">
    <xsl:choose>
    	<xsl:when test="sfdc:type = 'Picklist'">
    		<xsl:for-each select="sfdc:picklist/sfdc:picklistValues">
    			<xsl:value-of select="sfdc:fullName" />
    			<xsl:if test="sfdc:default = 'true'"> - default</xsl:if>
    			<xsl:text disable-output-escaping="yes">&amp;#13;</xsl:text>
    		</xsl:for-each>
    	</xsl:when>
    	<xsl:when test="sfdc:formula">
    		<xsl:value-of select="sfdc:formula" />
    	</xsl:when>
    	<xsl:when test="sfdc:type = 'Summary'">
    		<xsl:value-of select="sfdc:summaryOperation" /> - <b><xsl:value-of select="sfdc:summarizedField" /></b> on <b><xsl:value-of select="sfdc:summaryForeignKey" /></b>
    	</xsl:when>
    	<xsl:otherwise></xsl:otherwise>
    </xsl:choose>
    <!--
    picklist: picklist.picklistValues(fullName,default)&#13;summary:summarizedField,summaryForeignKey,summaryOperation&#13;function: formula
    -->
    </Data></Cell>

    <Cell ss:StyleID="s28"><Data ss:Type="String"><xsl:value-of select="sfdc:defaultValue" /></Data></Cell>
    <Cell ss:StyleID="s30"/>
    <Cell ss:StyleID="s28"><Data ss:Type="String">
    	<xsl:choose>
    		<xsl:when test="contains( sfdc:fullName, '__c' )">false</xsl:when>
    		<xsl:otherwise>true</xsl:otherwise>
    	</xsl:choose>
    </Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="String"><xsl:value-of select="sfdc:externalId" /></Data></Cell>
    <Cell ss:StyleID="s29"><Data ss:Type="String"><xsl:value-of select="sfdc:required" /></Data></Cell>
    <Cell ss:StyleID="s29"><Data ss:Type="String"><xsl:value-of select="sfdc:unique" /></Data></Cell>
    <Cell ss:StyleID="s29"><Data ss:Type="String"><xsl:value-of select="sfdc:trackHistory" /></Data></Cell>
    <Cell ss:StyleID="s28"><Data ss:Type="String"><xsl:value-of select="sfdc:description" /></Data></Cell>
    <Cell ss:StyleID="s30"/>
   </Row>
   </xsl:for-each>
   <Row>
    <Cell ss:Index="10" ss:StyleID="s23"/>
    <Cell ss:Index="13" ss:StyleID="s22"/>
    <Cell ss:StyleID="s24"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s26"><Data ss:Type="String">List View</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">List Name</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Filter Scope</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Columns</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Shared To</Data></Cell>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s31"/>
   </Row>
   <xsl:for-each select="sfdc:CustomObject/sfdc:listViews">
   <Row>
    <Cell ss:Index="2" ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:fullName" /></Data></Cell>
    <Cell ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:label" /></Data></Cell>
    <Cell ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:filterScope" /></Data></Cell>
    <Cell ss:StyleID="s33"><Data ss:Type="String">
    	<xsl:for-each select="sfdc:columns">
    		<xsl:sort select="sfdc:columns" />
    		<xsl:value-of select="text()" />
    		<xsl:text disable-output-escaping="yes">&amp;#13;</xsl:text>
    	</xsl:for-each>
    </Data></Cell>
    <Cell ss:StyleID="s33"><Data ss:Type="String">
    	<xsl:for-each select="sfdc:sharedTo/sfdc:group">
    		<xsl:value-of select="text()" />
    		<xsl:text disable-output-escaping="yes">&amp;#13;</xsl:text>
    	</xsl:for-each>
    </Data></Cell>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s28"/>
   </Row>
   </xsl:for-each>
   <Row>
    <Cell ss:Index="10" ss:StyleID="s23"/>
    <Cell ss:Index="13" ss:StyleID="s22"/>
    <Cell ss:StyleID="s24"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="23.0">
    <Cell ss:Index="2" ss:StyleID="s26"><Data ss:Type="String">Validation Rule</Data></Cell>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Is Active</Data></Cell>
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Error Message</Data></Cell>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s26"/>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s32"/>
    <Cell ss:StyleID="s26"><Data ss:Type="String">Description</Data></Cell>
    <Cell ss:StyleID="s31"/>
    <Cell ss:StyleID="Default"/>
   </Row>
   <xsl:for-each select="sfdc:CustomObject/sfdc:validationRules">
   <Row>
    <Cell ss:Index="2" ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:fullName" /></Data></Cell>
    <Cell ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:active" /></Data></Cell>
    <Cell ss:StyleID="s33"/>
    <Cell ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:errorMessage" /></Data></Cell>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s33"/>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s29"/>
    <Cell ss:StyleID="s33"><Data ss:Type="String"><xsl:value-of select="sfdc:description" /></Data></Cell>
    <Cell ss:StyleID="s28"/>
    <Cell ss:StyleID="Default"/>
   </Row>
   </xsl:for-each>
   <Row >
    <Cell ss:StyleID="Default"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="60.0"/>
   <Row ss:AutoFitHeight="0" ss:Height="23.0"/>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>-4</HorizontalResolution>
    <VerticalResolution>-4</VerticalResolution>
   </Print>
   <PageLayoutZoom>0</PageLayoutZoom>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>12</ActiveRow>
     <ActiveCol>6</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
</xsl:template>
</xsl:stylesheet>
